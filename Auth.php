<?php
  require_once 'connection.php';
  $response = array();
  if(isset($_GET['api'])){
  switch($_GET['api']){
  case 'signup':
    if(isTheseParametersAvailable(array('username','email','password','gender'))){
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = md5($_POST['password']);
    $gender = $_POST['gender'];

    $query = $conn->prepare("SELECT id FROM users WHERE username = ? OR email = ?");
    $query->bind_param("ss", $username, $email);
    $query->execute();
    $query->store_result();

    if($query->num_rows > 0){
        $response['error'] = true;
        $response['message'] = 'User already registered';
        $query->close();
    }
    else{
        $query = $conn->prepare("INSERT INTO users (username, email, password, gender) VALUES (?, ?, ?, ?)");
        $query->bind_param("ssss", $username, $email, $password, $gender);

        if($query->execute()){
            $query = $conn->prepare("SELECT id, id, username, email, gender FROM users WHERE username = ?");
            $query->bind_param("s",$username);
            $query->execute();
            $query->bind_result($userid, $id, $username, $email, $gender);
            $query->fetch();

            $user = array(
            'id'=>$id,
            'username'=>$username,
            'email'=>$email,
            'gender'=>$gender
            );

            $query->close();

            $response['error'] = false;
            $response['message'] = 'User registered successfully';
            $response['user'] = $user;
        }
    }

}
else{
    $response['error'] = true;
    $response['message'] = 'required parameters are not available';
}
break;
case 'login':
  if(isTheseParametersAvailable(array('username', 'password'))){
    $username = $_POST['username'];
    $password = md5($_POST['password']);

    $query = $conn->prepare("SELECT id, username, email, gender FROM users WHERE username = ? AND password = ?");
    $query->bind_param("ss",$username, $password);
    $query->execute();
    $query->store_result();
    if($query->num_rows > 0){
    $query->bind_result($id, $username, $email, $gender);
    $query->fetch();
    $user = array(
    'id'=>$id,
    'username'=>$username,
    'email'=>$email,
    'gender'=>$gender
    );

    $response['error'] = false;
    $response['message'] = 'Login successfull';
    $response['user'] = $user;
 }
 else{
    $response['error'] = false;
    $response['message'] = 'Invalid username or password';
 }
}
break;
default:
 $response['error'] = true;
 $response['message'] = 'Invalid Operation Called';
}
}
else{
 $response['error'] = true;
 $response['message'] = 'Error Api';
}
echo json_encode($response);
function isTheseParametersAvailable($params){
foreach($params as $param){
 if(!isset($_POST[$param])){
     return false;
  }
}
return true;
}
?>
